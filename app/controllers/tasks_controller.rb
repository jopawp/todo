class TasksController < ApplicationController

	def index
		@task = Task.new
		@tasks = Task.all
	end

	def create
		Task.create(task_params)
		redirect_to root_path
	end

	def new
	end

	def edit
		@tasks = Task.all
		@task = Task.find(params[:id])
	end

	def update
		task = Task.find(params[:id])
		task.update_attributes(task_params)
		redirect_to root_path
	end

	def find_by_category
		@cat_task = Task.find(params[:id])
		@tasks = Task.where("category = ?", @cat_task.category)
		render "index"
	end

	def destroy
		Task.destroy(params[:id])
		redirect_to root_path
	end

	def task_params
		params.require(:task).permit(:due_date, :item, :category)
	end

end
