class Task < ActiveRecord::Base
	validates :item, presence: true
	validates :due_date, presence: true
	validates :category, presence: true
	
end
