class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :category
      t.string :item
      t.date :due_date

      t.timestamps null: false
    end
  end
end
