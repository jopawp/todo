# README

   Todo

## Contributors

   Matt Weidner <jopawp@gmail.com>

## Description

   Maintain a simple list of tasks with category and due date for a single user.

   * Developed with Ruby 2.2.2 and Rails 4.
   * Uses Sqlite3 database.
   * Not tested with postgresql.

## Todo

   * Migrate to postgresql.
   * Implement e-mail or possibly SMS task due reminders.
   * Perhaps maintain a task history. Right now completed tasks are destroyed.
   * Multiuser access with proper login security.